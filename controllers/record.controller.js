const Record = require('../models/record.model.js');

// Retrieve and return all records from the database.
exports.findAll = (req, res) => {
  const { startDate, endDate, minCount, maxCount } = req.body;
  Record.aggregate([
    {
      $project: {
        key: "$key",
        createdAt: "$createdAt",
        totalCount: { $sum: "$counts" },
      }
    },
    {
      $match:
      {
        "createdAt": {
          "$gte": new Date(startDate),
          "$lte": new Date(endDate),
        },
        "totalCount": {
          "$gte": minCount,
          "$lte": maxCount,
        },
      }
    },
  ], function (err, result) {
    if (err) {
      res.send(err);
    } else {
      res.json({
        'code': 0,
        'msg': 'Success',
        'records': result,
      });
    }
  });

};

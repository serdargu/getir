const validator = require('../helpers/validate');

const record = (req, res, next) => {
  const validationRule = {
    "startDate": "required|date|before_or_equal:endDate",
    "endDate": "required|date|after_or_equal:startDate",
    "minCount": "required|integer|before_or_equal:maxCount",
    "maxCount": "required|integer|after_or_equal:minCount",
  }
  validator(req.body, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(412)
        .send({
          code: 1,
          msg: 'Validation failed',
          records: err
        });
    } else {
      next();
    }
  });
}

module.exports = {
  record
}
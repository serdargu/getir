# Getir RESTful API

A RESTful API with a single endpoint that fetches the data in the provided MongoDB collection and return the results in the requested format.

## API Documentation [here](https://getir-nodejs.herokuapp.com/api-document.html)

## API URL [here](https://getir-nodejs.herokuapp.com/records)

## Postman Export [here](https://documenter.getpostman.com/view/10087146/T1DjiyX3)

---
## Requirements

For development, you will only need Node.js and npm installed in your environement.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

---

## Install

    $ git clone https://gitlab.com/serdargu/getir.git
    $ cd getir
    $ npm install

## Configure app (changing the port)

Open `server.js` then edit the port, default is 5000.


## Running the project

    $ npm start

Then, visit the page localhost:5000, see if it works.

## Testing

    $ npm test

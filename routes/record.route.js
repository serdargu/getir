module.exports = (app) => {
  const records = require('../controllers/record.controller');
  const validation = require('../middlewares/record.middleware');
  app.post('/records', validation.record, records.findAll);
}
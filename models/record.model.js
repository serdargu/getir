const mongoose = require('mongoose');

const RecordSchema = mongoose.Schema({
  key: String,
  createdAt: Date,
  counts: [Number],
}, {
  timestamps: true
});

module.exports = mongoose.model('Record', RecordSchema);
const express = require('express');
const mongoose = require('mongoose');

const dbConfig = require('./configs/database.config.js');

var app = express();
app.use(express.json());
app.use(express.static('public'))

// Database connection
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url, {
  useNewUrlParser: true, useUnifiedTopology: true
}).then(() => {
  console.log("Successfully connected to the database");
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

require('./routes/record.route.js')(app);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`localhost:${port}`);
});
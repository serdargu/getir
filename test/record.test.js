process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Record = require('../models/record.model');

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('Records', () => {
  describe('/POST records', () => {
    it('it should give validation error', (done) => {
      let record = {};
      chai.request('http://localhost:5000')
        .post('/records')
        .send(record)
        .end((err, res) => {
          res.should.have.status(412);
          res.body.should.be.a('object');
          res.body.should.have.property('code').eql(1);
          res.body.should.have.property('msg').eql('Validation failed');
          res.body.should.have.property('records');
          done();
        });
    });
  });

  describe('/POST records', () => {
    it('it should give date validation error', (done) => {
      let record = {
        "startDate": "2018-02-03", "endDate": "2018-02-02", "minCount": 2700, "maxCount": 3000
      };
      chai.request('http://localhost:5000')
        .post('/records')
        .send(record)
        .end((err, res) => {
          res.should.have.status(412);
          res.body.should.be.a('object');
          res.body.should.have.property('code').eql(1);
          res.body.should.have.property('msg').eql('Validation failed');
          res.body.should.have.property('records');
          done();
        });
    });
  });

  describe('/POST records', () => {
    it('it should POST a record without any error', (done) => {
      let record = {
        "startDate": "2016-01-26", "endDate": "2018-02-02", "minCount": 2700, "maxCount": 3000
      };
      chai.request('http://localhost:5000')
        .post('/records')
        .send(record)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('code').eql(0);
          res.body.should.have.property('msg').eql('Success');
          res.body.should.have.property('records');
          done();
        });
    });
  });

});
